package com.example.horairestrain_tp1e8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;

public class AffichageHoraireActivity extends AppCompatActivity {

    public static final String DEPART = "depart";
    public static final String ARRIVEE = "arrivee";

    private LinearLayout myLayout2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affichage_horaire);
        this.myLayout2 = (LinearLayout) findViewById(R.id.layoutDinamique2);
        Intent monIntent = getIntent();

        String depart = monIntent.getStringExtra(DEPART);
        String arrivee = monIntent.getStringExtra(ARRIVEE);

        TextView tvPrompt = new TextView(this);
        tvPrompt.setText("Voici les horaire au départ de "+depart+" et a destination de "+arrivee);
        myLayout2.addView(tvPrompt);

        Hashtable<String, ArrayList<String>> Horaire = new Hashtable();
        ArrayList<String> MtpPa = new ArrayList<>();
        ArrayList<String> MtpLy = new ArrayList<>();
        ArrayList<String> LyPa = new ArrayList<>();
        ArrayList<String> LyMtp = new ArrayList<>();
        ArrayList<String> PaLy = new ArrayList<>();
        ArrayList<String> PaMtp = new ArrayList<>();


        Horaire.put("Montpellier Paris", MtpPa);
        Horaire.put("Montpellier Lyon", MtpLy);
        Horaire.put("Lyon Paris", LyPa);
        Horaire.put("Lyon Montpellier", LyMtp);
        Horaire.put("Paris Lyon", PaLy);
        Horaire.put("Paris Montpellier", PaMtp);

        MtpPa.add("09h00 13h02");
        MtpPa.add("11h17 15h18");
        MtpLy.add("09h34 11h20");
        MtpLy.add("17h17 19h28");
        LyPa.add("09h05 11h02");
        LyPa.add("20h17 22h31");
        LyMtp.add("07h09 11h02");
        LyMtp.add("12h03 14h28");
        PaLy.add("07h11 09h02");
        PaLy.add("11h44 13h28");
        PaMtp.add("06h00 10h02");
        PaMtp.add("11h20 15h02");



        String key=depart+" "+arrivee;
        ListView resHoraire = new ListView(this);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,Horaire.get(key));
        resHoraire.setAdapter(arrayAdapter);
        myLayout2.addView(resHoraire);

    }
}