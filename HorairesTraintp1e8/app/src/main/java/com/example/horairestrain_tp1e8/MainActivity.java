package com.example.horairestrain_tp1e8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private LinearLayout myLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.myLayout = (LinearLayout) findViewById(R.id.layoutDinamique);

        TextView prompt1 = new TextView(this);
        prompt1.setText("Entrez votre ville de départ, \"Montpellier\", \"Paris\" ou \"Lyon\" " );
        myLayout.addView(prompt1);
        EditText edVilleDepart = new EditText(this);
        edVilleDepart.setHint("Ville de départ");
        myLayout.addView(edVilleDepart);

        TextView prompt2 = new TextView(this);
        prompt2.setText("Entrez votre ville d'arrivé, \"Montpellier\", \"Paris\" ou \"Lyon\" " );
        myLayout.addView(prompt2);

        EditText edVilleArrivee = new EditText(this);
        edVilleArrivee.setHint("Ville d'arrivé");
        myLayout.addView(edVilleArrivee);

        Button sendButton  = new Button(this);
        sendButton.setText("Cherche les horaire");

        sendButton.setOnClickListener(new View.OnClickListener(){
            String depart="";
            String arrivee="";
            Boolean ok=false;
            Boolean depConnu=false;
            Boolean arrConnu=false;
            Boolean depDiffArr=false;

            @Override
            public void onClick(View v) {
                if(!ok) { //tant que les données ne sont pas bonne
                    depart = edVilleDepart.getText().toString();
                    arrivee = edVilleArrivee.getText().toString();
                    if (!depart.equals(arrivee)) depDiffArr = true;
                    else depDiffArr = false; //on verifie que depart et arrive soit differente

                    if(depart.equals("Montpellier") || depart.equals("Paris") || depart.equals("Lyon")) depConnu = true;
                    else depConnu = false;

                    if(arrivee.equals("Montpellier") || arrivee.equals("Paris") || arrivee.equals("Lyon")) arrConnu = true;
                    else arrConnu = false;

                    if(!depDiffArr) Toast.makeText(MainActivity.this,"Les villes de départ et d'arrivée sont identique",Toast.LENGTH_LONG).show();
                    if(!depConnu) Toast.makeText(MainActivity.this,"Je ne connais pas la ville de départ",Toast.LENGTH_LONG).show();
                    if(!arrConnu) Toast.makeText(MainActivity.this,"Je ne connais pas la ville d'arrivée",Toast.LENGTH_LONG).show();

                    ok=arrConnu&&depConnu&&depDiffArr;



                }
                if(ok) {//ici on verifie bien if(ok) et pas else, au cas ou la première tentative est valide
                    ok=false;//on remmet ok à false au cas ou on reviens sur la première activité depuis la 2eme
                    Intent intentItineraire = new Intent(MainActivity.this, AffichageHoraireActivity.class);
                    intentItineraire.putExtra(AffichageHoraireActivity.DEPART, depart);
                    intentItineraire.putExtra(AffichageHoraireActivity.ARRIVEE, arrivee);
                    startActivity(intentItineraire);
                }
            }


        });

        myLayout.addView(sendButton);

    }
}