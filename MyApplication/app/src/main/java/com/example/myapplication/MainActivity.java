
package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private LinearLayout myLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        this.myLayout = (LinearLayout) findViewById(R.id.layoutDinamique);
        //ArrayList<String> champ = new ArrayList<String>();
        //champ.add("name");

        TextView tvName = new TextView(this);
        tvName.setText(getResources().getString(R.string.name));
        myLayout.addView(tvName);
        EditText edName = new EditText(this);
        edName.setHint(getResources().getString(R.string.name));
        myLayout.addView(edName);

        TextView tvLastName = new TextView(this);
        tvLastName.setText(getResources().getString(R.string.first_name));
        myLayout.addView(tvLastName);
        EditText edLastName = new EditText(this);
        edLastName.setHint(getResources().getString(R.string.first_name));
        myLayout.addView(edLastName);

        TextView tvAge = new TextView(this);
        tvAge.setText(getResources().getString(R.string.age));
        myLayout.addView(tvAge);
        EditText edAge = new EditText(this);
        edAge.setHint(getResources().getString(R.string.age));
        edAge.setInputType(InputType.TYPE_CLASS_NUMBER);
        myLayout.addView(edAge);

        TextView tvDomain = new TextView(this);
        tvDomain.setText(getResources().getString(R.string.competance_domain));
        myLayout.addView(tvDomain);
        EditText edDomain = new EditText(this);
        edDomain.setHint(getResources().getString(R.string.competance_domain));
        myLayout.addView(edDomain);

        TextView tvPhoneNumber = new TextView(this);
        tvPhoneNumber.setText(getResources().getString(R.string.phone_number));
        myLayout.addView(tvPhoneNumber);
        EditText edPhoneNumber = new EditText(this);
        edPhoneNumber.setHint(getResources().getString(R.string.phone_number));
        edPhoneNumber.setInputType(InputType.TYPE_CLASS_PHONE);
        myLayout.addView(edPhoneNumber);

        Button bouton = new Button(this);
        bouton.setText(getResources().getString(R.string.validate));
        //myLayout.addView(bouton);
        bouton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Voulez vous valider la saisie ?");
                builder.setPositiveButton("Oui !",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(MainActivity.this,"D'accord",Toast.LENGTH_LONG).show();
                                Intent myIntent1 = new Intent(MainActivity.this, deuxieme_act.class);
                                myIntent1.putExtra(deuxieme_act.NAME,edName.getText().toString()); //ajout du nom
                                myIntent1.putExtra(deuxieme_act.LASTNAME,edLastName.getText().toString()); //ajout du prénom
                                myIntent1.putExtra(deuxieme_act.AGE,edAge.getText().toString());//ajout de l'age
                                myIntent1.putExtra(deuxieme_act.DOMAIN,edDomain.getText().toString());//ajout du domaine
                                myIntent1.putExtra(deuxieme_act.TELEPHONE,edPhoneNumber.getText().toString());//ajout du num de tel
                                startActivity(myIntent1);
                            }
                        });
                builder.setNegativeButton("Non !",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(MainActivity.this,"Très bien, j'annule",Toast.LENGTH_LONG).show();
                            }
                        });
                builder.show();

            }
        });
        myLayout.addView(bouton);

    }
}