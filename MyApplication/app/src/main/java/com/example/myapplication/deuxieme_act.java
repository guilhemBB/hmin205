package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class deuxieme_act extends AppCompatActivity {


    public static final String NAME = "name";
    public static final String LASTNAME = "lastname";
    public static final String AGE = "age";
    public static final String DOMAIN = "domain";
    public static final String TELEPHONE = "telephon";

    private LinearLayout myLayout2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deuxieme_act);
        this.myLayout2 = (LinearLayout) findViewById(R.id.layoutDinamique2);
        Intent monIntent2 = getIntent();

        String name= monIntent2.getStringExtra(NAME);
        TextView tvName = new TextView(this);
        tvName.setText(name);
        myLayout2.addView(tvName);

        String lastname= monIntent2.getStringExtra(LASTNAME);
        TextView tvLastName = new TextView(this);
        tvLastName.setText(lastname);
        myLayout2.addView(tvLastName);

        String age= monIntent2.getStringExtra(AGE);
        TextView tvAge = new TextView(this);
        tvAge.setText(age);
        myLayout2.addView(tvAge);

        String domain= monIntent2.getStringExtra(DOMAIN);
        TextView tvDomain = new TextView(this);
        tvDomain.setText(domain);
        myLayout2.addView(tvDomain);

        String telephon= monIntent2.getStringExtra(TELEPHONE);
        TextView tvTelephon = new TextView(this);
        tvTelephon.setText(telephon);
        myLayout2.addView(tvTelephon);

        //-------
        Button bouton1 = new Button(this);
        bouton1.setText("OK");
        bouton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntentPasse = new Intent(deuxieme_act.this, troisieme_act.class);
                myIntentPasse.putExtra(troisieme_act.TELEPHONE,telephon);//ajout du num de tel
                startActivity(myIntentPasse);
            }
        });
        myLayout2.addView(bouton1);

        Button bouton2 = new Button(this);
        bouton2.setText(getResources().getString(R.string.txt_return));
        bouton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntentAnnule = new Intent(deuxieme_act.this, MainActivity.class);
                startActivity(myIntentAnnule);
            }
        });
        myLayout2.addView(bouton2);
        //-------
        }

}