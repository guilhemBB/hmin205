package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class troisieme_act extends AppCompatActivity {

    public static final String TELEPHONE = "telephon";

    private LinearLayout myLayout3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_troisieme_act);
        this.myLayout3 = (LinearLayout) findViewById(R.id.layoutDinamique3);
        Intent monIntent3 = getIntent();

        String telephon= monIntent3.getStringExtra(TELEPHONE);
        TextView tvTelephon = new TextView(this);
        tvTelephon.setText(telephon);
        myLayout3.addView(tvTelephon);

        ActivityCompat.requestPermissions(troisieme_act.this,
                new String[]{Manifest.permission.CALL_PHONE},
                1);

        Button boutonAppel = new Button(this);
        boutonAppel.setText(getResources().getString(R.string.call));
        boutonAppel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "tel:"+telephon;
                Intent myIntentAppel= new Intent(Intent.ACTION_CALL);
                myIntentAppel.setData(Uri.parse(uri));
                startActivity(myIntentAppel);
            }
        });
        myLayout3.addView(boutonAppel);

        /* Licence image TDLR
         * voir https://commons.wikimedia.org/wiki/File:Telephone_model_PTT24-IMG_9919.jpg
         *
         * Creative Commons attribution share alike
         * This file is licensed under the Creative Commons Attribution-Share Alike 2.0 France license.
         * Attribution: Photograph by Rama, Wikimedia Commons, Cc-by-sa-2.0-fr
         * Rama profil : https://commons.wikimedia.org/wiki/User:Rama
         * You are free:
         * to share – to copy, distribute and transmit the work
         * to remix – to adapt the work
         *
         * Under the following conditions:
         *
         * attribution – You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
         * share alike – If you remix, transform, or build upon the material, you must distribute your contributions under the same or compatible license as the original.
         * */

        ImageView ptt24 = new ImageView(this);
        ptt24.setImageResource(R.drawable.ptt24);
        myLayout3.addView(ptt24);
    }
}